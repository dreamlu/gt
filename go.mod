module github.com/dreamlu/gt

go 1.15

require (
	github.com/bwmarrin/snowflake v0.3.0
	github.com/bxcodec/faker/v3 v3.6.0
	github.com/dreamlu/resize v0.0.0-20180221191011-83c6a9932646
	github.com/go-redis/redis/v8 v8.11.0
	github.com/jonboulle/clockwork v0.2.2 // indirect
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.5 // indirect
	github.com/nsqio/go-nsq v1.0.8
	github.com/pkg/errors v0.9.1
	github.com/rifflock/lfshook v0.0.0-20180920164130-b9218ef580f5
	github.com/sirupsen/logrus v1.8.1
	go.mongodb.org/mongo-driver v1.6.0
	gopkg.in/yaml.v2 v2.4.0
	gorm.io/driver/mysql v1.1.1
	gorm.io/gorm v1.21.12
)
